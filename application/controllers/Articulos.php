<?php
    error_reporting(0);
  /**
   *
   */
  class Articulos extends CI_Controller
  {

    function __construct(){
      parent::__construct();
      $this->load->model("Articulo");


    }

    public function index(){
      $data["listadoArticulos"]=$this->Articulo->consultarTodos();
      $this->load->view('header');
      $this->load->view('articulos/index',$data);
      $this->load->view('footer');
    }

    //Eliminacion de hospitales con get ID
    public function borrar($id){
      $this->Articulo->eliminar($id);
      $this->session->set_flashdata("confirmacion","Articulo eliminado exitosamente");
      redirect("articulos/index");
    }

    //Renderización formulario de nuevo Hospital
    public function nuevo(){
      $data["listadoArticulos"]=$this->Articulo->consultarTodos();
      $this->load->view("header");
      $this->load->view("articulos/nuevo",$data);
      $this->load->view("footer");
    }

    //capturando datos e insertanfo en la tabla
    public function guardarArticulo(){

      $datosNuevoArticulo=array(
        "id_art"=>$this->input->post("id_art"),
        "titulo_art"=>$this->input->post("titulo_art"),
        "fecha_art"=>$this->input->post("fecha_art"),
        "volumen_art"=>$this->input->post("volumen_art"),
        "numero_art"=>$this->input->post("numero_art"),
        "url_art"=>$this->input->post("url_art")

      );
      $this->Articulo->insertar($datosNuevoArticulo);
      $this->session->set_flashdata("confirmacion","Articulo guardado exitosamente");
      redirect('articulos/index');

    }


    //editar
    public function editar($id){
      $data["articuloEditar"]=$this->Articulo->obtenerPorId($id);
      $this->load->view('header');
      $this->load->view('articulos/editar',$data);
      $this->load->view('footer');
    }

    public function actualizarArticulo(){
      $id=$this->input->post("id_art");
      $datosArticulo=array(
        "id_art"=>$this->input->post("id_art"),
        "titulo_art"=>$this->input->post("titulo_art"),
        "fecha_art"=>$this->input->post("fecha_art"),
        "volumen_art"=>$this->input->post("volumen_art"),
        "numero_art"=>$this->input->post("numero_art"),
        "url_art"=>$this->input->post("url_art")

      );
      $this->Articulo->actualizar($id, $datosArticulo);
      $this->session->set_flashdata("confirmacion","Articulo actualizado exitosamente");
      redirect('articulos/index');
    }




  }// fin de clase


 ?>