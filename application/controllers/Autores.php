<?php
    error_reporting(0);
  /**
   *
   */
  class Autores extends CI_Controller
  {

    function __construct(){
      parent::__construct();
      $this->load->model("Autor");


    }

    public function index(){
      $data["listadoAutores"]=$this->Autor->consultarTodos();
      $this->load->view('header');
      $this->load->view('autores/index',$data);
      $this->load->view('footer');
    }

    //Eliminacion de hospitales con get ID
    public function borrar($id){
      $this->Autor->eliminar($id);
      $this->session->set_flashdata("confirmacion","Autor eliminado exitosamente");
      redirect("autores/index");
    }

    //Renderización formulario de nuevo Hospital
    public function nuevo(){
      $data["listadoAutores"]=$this->Autor->consultarTodos();
      $this->load->view("header");
      $this->load->view("autores/nuevo",$data);
      $this->load->view("footer");
    }

    //capturando datos e insertanfo en la tabla
    public function guardarAutor(){

      $datosNuevoAutor=array(
        "id_aut"=>$this->input->post("id_aut"),
        "apellidos_aut"=>$this->input->post("apellidos_aut"),
        "nombres_aut"=>$this->input->post("nombres_aut")

      );
      $this->Autor->insertar($datosNuevoAutor);
      $this->session->set_flashdata("confirmacion","Autor guardado exitosamente");
      redirect('autores/index');

    }


    //editar
    public function editar($id){
      $data["autorEditar"]=$this->Autor->obtenerPorId($id);
      $this->load->view('header');
      $this->load->view('autores/editar',$data);
      $this->load->view('footer');
    }

    public function actualizarAutor(){
      $id=$this->input->post("id_aut");
      $datosAutor=array(
        "id_aut"=>$this->input->post("id_aut"),
        "apellidos_aut"=>$this->input->post("apellidos_aut"),
        "nombres_aut"=>$this->input->post("nombres_aut")

      );
      $this->Autor->actualizar($id, $datosAutor);
      $this->session->set_flashdata("confirmacion","Autor actualizado exitosamente");
      redirect('autores/index');
    }




  }// fin de clase


 ?>
