<?php
    error_reporting(0);
  /**
   *
   */
  class Directores extends CI_Controller
  {

    function __construct(){
      parent::__construct();
      $this->load->model("Director");


    }

    public function index(){
      $data["listadoDirectores"]=$this->Director->consultarTodos();
      $this->load->view('header');
      $this->load->view('directores/index',$data);
      $this->load->view('footer');
    }

    //Eliminacion de hospitales con get ID
    public function borrar($id){
      $this->Director->eliminar($id);
      $this->session->set_flashdata("confirmacion","Director eliminado exitosamente");
      redirect("directores/index");
    }

    //Renderización formulario de nuevo Hospital
    public function nuevo(){
      $data["listadoDirectores"]=$this->Director->consultarTodos();
      $this->load->view("header");
      $this->load->view("directores/nuevo",$data);
      $this->load->view("footer");
    }

    //capturando datos e insertanfo en la tabla
    public function guardarDirector(){

      $datosNuevoDirector=array(
        "id_dir"=>$this->input->post("id_dir"),
        "nombre_dir"=>$this->input->post("nombre_dir"),
        "apellido_dir"=>$this->input->post("apellido_dir"),
        "correo_dir"=>$this->input->post("correo_dir")

      );
      $this->Director->insertar($datosNuevoDirector);
      $this->session->set_flashdata("confirmacion","Director guardado exitosamente");
      redirect('directores/index');

    }


    //editar
    public function editar($id){
      $data["directorEditar"]=$this->Director->obtenerPorId($id);
      $this->load->view('header');
      $this->load->view('directores/editar',$data);
      $this->load->view('footer');
    }

    public function actualizarDirector(){
      $id=$this->input->post("id_dir");
      $datosDirector=array(
        "id_dir"=>$this->input->post("id_dir"),
        "nombre_dir"=>$this->input->post("nombre_dir"),
        "apellido_dir"=>$this->input->post("apellido_dir"),
        "correo_dir"=>$this->input->post("correo_dir")

      );
      $this->Director->actualizar($id, $datosDirector);
      $this->session->set_flashdata("confirmacion","Director actualizado exitosamente");
      redirect('directores/index');
    }




  }// fin de clase


 ?>
