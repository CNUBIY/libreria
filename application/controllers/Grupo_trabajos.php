<?php
    error_reporting(0);
  /**
   *
   */
  class Grupo_trabajos extends CI_Controller
  {

    function __construct(){
      parent::__construct();
      $this->load->model("Grupo_trabajo");


    }

    public function index(){
      $data["listadoGrupo_trabajos"]=$this->Grupo_trabajo->consultarTodos();
      $this->load->view('header');
      $this->load->view('grupo_trabajos/index',$data);
      $this->load->view('footer');
    }

    //Eliminacion de hospitales con get ID
    public function borrar($id){
      $this->Grupo_trabajo->eliminar($id);
      $this->session->set_flashdata("confirmacion","Grupo_trabajo eliminado exitosamente");
      redirect("grupo_trabajos/index");
    }

    //Renderización formulario de nuevo Hospital
    public function nuevo(){
      $data["listadoGrupo_trabajos"]=$this->Grupo_trabajo->consultarTodos();
      $data["listadoAutores"]=$this->Grupo_trabajo->consultarAutores();
      $data["listadoArticulos"]=$this->Grupo_trabajo->consultarArticulos();
      $this->load->view("header");
      $this->load->view("grupo_trabajos/nuevo",$data);
      $this->load->view("footer");
    }

    //capturando datos e insertanfo en la tabla
    public function guardarGrupo_trabajo(){

      $datosNuevoGrupo_trabajo=array(
        "fk_id_aut"=>$this->input->post("fk_id_aut"),
        "fk_id_art"=>$this->input->post("fk_id_art"),
        "id_gru"=>$this->input->post("id_gru")
      );
      $this->Grupo_trabajo->insertar($datosNuevoGrupo_trabajo);
      $this->session->set_flashdata("confirmacion","Grupo_trabajo guardado exitosamente");
      redirect('grupo_trabajos/index');

    }


    //editar
    public function editar($id){
      $data["grupo_trabajoEditar"]=$this->Grupo_trabajo->obtenerPorId($id);
      $data["listadoAutores"]=$this->Grupo_trabajo->consultarTodos();
      $data["listadoArticulos"]=$this->Grupo_trabajo->consultarTodos();
      $this->load->view('header');
      $this->load->view('grupo_trabajos/editar',$data);
      $this->load->view('footer');
    }

    public function actualizarGrupo_trabajo(){
      $id=$this->input->post("id_gru");
      $datosGrupo_trabajo=array(
        "fk_id_aut"=>$this->input->post("fk_id_aut"),
        "fk_id_art"=>$this->input->post("fk_id_art"),
        "id_gru"=>$this->input->post("id_gru")

      );
      $this->Grupo_trabajo->actualizar($id, $datosGrupo_trabajo);
      $this->session->set_flashdata("confirmacion","Grupo_trabajo actualizado exitosamente");
      redirect('grupo_trabajos/index');
    }



  }// fin de clase


 ?>
