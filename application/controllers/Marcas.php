<?php
    error_reporting(0);
  /**
   *
   */
  class Marcas extends CI_Controller
  {

    function __construct(){
      parent::__construct();
      $this->load->model("Marca");


    }

    public function index(){
      $data["listadoMarca"]=$this->Marca->consultarTodos();
      $this->load->view('header');
      $this->load->view('marcas/index',$data);
      $this->load->view('footer');
    }

    //Eliminacion de hospitales con get ID
    public function borrar($id){
      $this->Marca->eliminar($id);
      $this->session->set_flashdata("confirmacion","Marca eliminado exitosamente");
      redirect("marcas/index");
    }

    //Renderización formulario de nuevo Hospital
    public function nuevo(){
      $data["listadoMarcas"]=$this->Marca->consultarTodos();
      $this->load->view("header");
      $this->load->view("marcas/nuevo",$data);
      $this->load->view("footer");
    }

    //capturando datos e insertanfo en la tabla
    public function guardarMarca(){

      $config['upload_path']=APPPATH.'../uploads/marcas/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      $nombre_aleatorio="logo_".time()*rand(100,10000);//creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      $this->load->library('upload',$config);//cargando la libreria UPLOAD
      if($this->upload->do_upload("logo_mar")){ //intentando subir el archivo
          $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
          $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
        }else{
          $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
        }

      $datosNuevoMarca=array(

        "id_mar"=>$this->input->post("id_mar"),
        "nombre_mar"=>$this->input->post("nombre_mar"),
        "logo_mar"=>$nombre_archivo_subido

      );
      $this->Marca->insertar($datosNuevoMarca);
      $this->session->set_flashdata("confirmacion","Marca guardado exitosamente");
      redirect('marcas/index');

    }


    //editar
    public function editar($id_mar){
      $data["marcaEditar"]=
      $this->Marca->obtenerPorId($id_mar);
      $this->load->view('header');
      $this->load->view('marcas/editar',$data);
      $this->load->view('footer');
    }

    public function actualizarMarca(){

      $config['upload_path']=APPPATH.'../uploads/marcas/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      $nombre_aleatorio="doctor_".time()*rand(100,10000);//creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      $this->load->library('upload',$config);//cargando la libreria UPLOAD
      if($this->upload->do_upload("logo_mar")){ //intentando subir el archivo
          $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
          $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
        }else{
          $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
        }

      $id=$this->input->post("id_mar");
      $datosMarca=array(
        "id_mar"=>$this->input->post("id_mar"),
        "nombre_mar"=>$this->input->post("nombre_mar"),
        "logo_mar"=>$nombre_archivo_subido

      );
      $this->Marca->actualizar($id, $datosMarca);
      $this->session->set_flashdata("confirmacion","Marca actualizado exitosamente");
      redirect('marcas/index');
    }




  }// fin de clase


 ?>
