<?php
error_reporting(0);

class Patrocinios extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Patrocinio");
        $this->load->model("Marca"); // Agregado
        $this->load->model("Revista"); // Agregado
    }

    public function index()
    {
        $data["listadoPatrocinios"] = $this->Patrocinio->consultarTodos();
        $this->load->view('header');
        $this->load->view('patrocinios/index', $data);
        $this->load->view('footer');
    }

    public function borrar($id){
      $this->Patrocinio->eliminar($id);
      $this->session->set_flashdata("confirmacion","patrocinio eliminado exitosamente");
      redirect("patrocinios/index");
    }


    public function nuevo()
    {
        $data["listadoMarca"] = $this->Patrocinio->consultarMarca(); // Corregido
        $data["listadoRevistas"] = $this->Patrocinio->consultarRevista(); // Corregido
        $data["listadoPatrocinios"] = $this->Patrocinio->consultarTodos();
        $this->load->view("header");
        $this->load->view("patrocinios/nuevo", $data);
        $this->load->view("footer");
    }

    public function guardarPatrocinio()
    {
        $datosNuevoPatrocinio = array(
            "fk_id_mar" => $this->input->post("fk_id_mar"), // Cambiado a "id_mar"
            "fk_id_rev" => $this->input->post("fk_id_rev")  // Cambiado a "id_rev"
        );
        $this->Patrocinio->insertar($datosNuevoPatrocinio);
        $this->session->set_flashdata("confirmacion", "Patrocinador guardado exitosamente");
        redirect('patrocinios/index');
    }


    public function editar($id)
    {
        $data["patrocinioEditar"] = $this->Patrocinio->obtenerPorId($id);
        $data["listadoMarca"] = $this->Marca->consultarTodos(); // Corregido
        $data["listadoRevistas"] = $this->Revista->consultarTodos(); // Corregido
        $this->load->view('header');
        $this->load->view('patrocinios/editar', $data);
        $this->load->view('footer');
    }
    public function actualizarPatrocinio()
    {
        $id = $this->input->post("id_pat");
        $datosPatrocinio = array(
            "fk_id_mar" => $this->input->post("fk_id_mar"),
            "fk_id_rev" => $this->input->post("fk_id_rev")
        );
        $this->Patrocinio->actualizar($id, $datosPatrocinio);
        $this->session->set_flashdata("confirmacion", "Patrocinadores actualizado exitosamente");
        redirect('patrocinios/index');
    }


}
