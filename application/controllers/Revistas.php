<?php
error_reporting(0);

class Revistas extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Revista");
    }

    public function index()
    {
        $data["listadoRevistas"] = $this->Revista->consultarTodos();
        $this->load->view('header');
        $this->load->view('revistas/index', $data);
        $this->load->view('footer');
    }

    public function borrar($id)
    {
        $this->Revista->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Revista eliminada exitosamente");
        redirect("revistas/index");
    }

    public function nuevo()
    {
        $data["listadoAutores"] = $this->Revista->consultarAutores();
        $data["listadoDirectores"] = $this->Revista->consultarDirectores();
        $data["listadoRevistas"] = $this->Revista->consultarTodos();
        $this->load->view("header");
        $this->load->view("revistas/nuevo", $data);
        $this->load->view("footer");
    }

    public function guardarRevista()
    {
        $datosNuevoRevista = array(
            "fk_id_aut" => $this->input->post("fk_id_aut"),
            "fk_id_dir" => $this->input->post("fk_id_dir"),
            "nombre_rev" => $this->input->post("nombre_rev")
        );
        $this->Revista->insertar($datosNuevoRevista);
        $this->session->set_flashdata("confirmacion", "Revista guardada exitosamente");
        redirect('revistas/index');
    }


    public function editar($id)
    {
        $data["revistaEditar"] = $this->Revista->obtenerPorId($id);
        $data["listadoAutores"] = $this->Revista->consultarTodos();
        $data["listadoDirectores"] = $this->Revista->consultarTodos();
        $data["listadoRevistas"] = $this->Revista->consultarTodos();
        $this->load->view('header');
        $this->load->view('revistas/editar', $data);
        $this->load->view('footer');
    }

    public function actualizarRevista()
    {
        $id = $this->input->post("id_rev");
        $datosRevista = array(
            "fk_id_aut" => $this->input->post("fk_id_aut"),
            "fk_id_dir" => $this->input->post("fk_id_dir"),
            "nombre_rev"=>$this->input->post("nombre_rev")


        );
        $this->Revista->actualizarRevista($id, $datosRevista);
        $this->session->set_flashdata("confirmacion", "Revista actualizada exitosamente");
        redirect('revistas/index');
    }
}

?>
