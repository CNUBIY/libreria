<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  error_reporting(0);
class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 public function __construct()
	 {
			 parent::__construct();
			 $this->load->model("Welcom");
	 }

	public function index()
	{
				$data["listadoArticulos"] = $this->Welcom->consultarTodos();
        $this->load->view('header');
        $this->load->view('welcome_message',$data);
        $this->load->view('footer');
	}

	public function reporte($id_art)
	{
    $data["listadoArticulos"] = $this->Welcom->obtenerPorId($id_art);
    // Cargar la vista y pasar los datos
    $this->load->view('header');
    $this->load->view('reporte', $data);
    $this->load->view('footer');
	}
}
