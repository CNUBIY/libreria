<?php
  /**
   *
   */
  class Articulo extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    
    function insertar($datos){
      $respuesta=$this->db->insert("articulo",$datos);
      return $respuesta;
    }

    //consulta de datos
    function consultarTodos(){
      $this->db->order_by("id_art","asc");
      $result=$this->db->get("articulo");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;//cuando no hay datos
      }
    }


    //eliminar datos
    function eliminar($id){
      $this->db->where("id_art",$id);
      return $this->db->delete("articulo");
    }



    //consulta de datos
    function obtenerPorId($id){
      $this->db->where("id_art",$id);
      $articulos=$this->db->get("articulo");
      if($articulos->num_rows()>0){
        return $articulos->row();
      } else {
        return false;
      }
    }


    function actualizar($id,$datos){
      $this->db->where("id_art",$id);
      return $this->db->update("articulo",$datos);
    }
 









  }

 ?>