<?php
  /**
   *
   */
  class Autor extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    //nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("autores",$datos);
      return $respuesta;
    }

    //consulta de datos
    function consultarTodos(){
      $this->db->order_by("id_aut","asc");
      $result=$this->db->get("autores");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;//cuando no hay datos
      }
    }


    //eliminar datos
    function eliminar($id){
      $this->db->where("id_aut",$id);
      return $this->db->delete("autores");
    }



    //consulta de datos
    function obtenerPorId($id){
      $this->db->where("id_aut",$id);
      $autores=$this->db->get("autores");
      if($autores->num_rows()>0){
        return $autores->row();
      } else {
        return false;
      }
    }


    function actualizar($id,$datos){
      $this->db->where("id_aut",$id);
      return $this->db->update("autores",$datos);
    }










  }

 ?>
