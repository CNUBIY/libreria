<?php
  /**
   *
   */
  class Director extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    //nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("director",$datos);
      return $respuesta;
    }

    //consulta de datos
    function consultarTodos(){
      $this->db->order_by("id_dir","asc");
      $result=$this->db->get("director");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;//cuando no hay datos
      }
    }


    //eliminar datos
    function eliminar($id){
      $this->db->where("id_dir",$id);
      return $this->db->delete("director");
    }



    //consulta de datos
    function obtenerPorId($id){
      $this->db->where("id_dir",$id);
      $directores=$this->db->get("director");
      if($directores->num_rows()>0){
        return $directores->row();
      } else {
        return false;
      }
    }


    function actualizar($id,$datos){
      $this->db->where("id_dir",$id);
      return $this->db->update("director",$datos);
    }










  }

 ?>
