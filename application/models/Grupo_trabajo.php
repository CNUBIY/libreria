<?php
  /**
   *
   */
  class Grupo_trabajo extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    //nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("grupo_trabajo",$datos);
      return $respuesta;
    }

    function consultarTodos(){
      $this->db->select('grupo_trabajo.*, autores.nombres_aut as nombre_autor');
      $this->db->select('grupo_trabajo.*, articulo.titulo_art as titulo_art');
      $this->db->from('grupo_trabajo');
      $this->db->join('autores', 'grupo_trabajo.fk_id_aut = autores.id_aut', 'left');
      $this->db->join('articulo', 'grupo_trabajo.fk_id_art = articulo.id_art', 'left');
      $query = $this->db->get();

      if($query->num_rows()>0){
        return $query->result();
      } else {
        return false;
      }
    }

    public function consultarAutores()
    {
        $this->db->select('id_aut, apellidos_aut');
        $this->db->select('id_aut, nombres_aut');
        $query = $this->db->get("autores");
        return $query->result();
    }

    public function consultarArticulos()
    {
        $this->db->select('id_art, titulo_art');
        $query = $this->db->get("articulo");
        return $query->result();
    }



    //eliminar datos
    function eliminar($id){
      $this->db->where("id_gru",$id);
      return $this->db->delete("grupo_trabajo");
    }



    //consulta de datos
    function obtenerPorId($id){
      $this->db->where("id_gru",$id);
      $grupo_trabajos=$this->db->get("grupo_trabajo");
      if($grupo_trabajos->num_rows()>0){
        return $grupo_trabajos->row();
      } else {
        return false;
      }
    }


    function actualizar($id,$datos){
      $this->db->where("id_gru",$id);
      return $this->db->update("grupo_trabajo",$datos);
    }

  }

 ?>
