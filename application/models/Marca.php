<?php
  /**
   *
   */
  class Marca extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    //nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("marca",$datos);
      return $respuesta;
    }

    //consulta de datos
    function consultarTodos(){
      $this->db->order_by("id_mar","asc");
      $result=$this->db->get("marca");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;//cuando no hay datos
      }
    }


    //eliminar datos
    function eliminar($id){
      $this->db->where("id_mar",$id);
      return $this->db->delete("marca");
    }



    //consulta de datos
    function obtenerPorId($id){
      $this->db->where("id_mar",$id);
      $marca=$this->db->get("marca");
      if($marca->num_rows()>0){
        return $marca->row();
      } else {
        return false;
      }
    }


    function actualizar($id,$datos){
      $this->db->where("id_mar",$id);
      return $this->db->update("marca",$datos);
    }










  }

 ?>