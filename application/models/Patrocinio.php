<?php
/**
 *
 */
class Patrocinio extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    // Insertar nuevos datos de patrocinio
    function insertar($datos){
        $respuesta=$this->db->insert("patrocinio",$datos);
        return $respuesta;
    }

    // Consultar todos los patrocinios con sus marcas y revistas
    // Consultar todos los patrocinios con sus marcas y revistas
    // Consultar todos los patrocinios con sus marcas y revistas
    function consultarTodos(){
      $this->db->select('patrocinio.*, revista.nombre_rev as nombre_rev');
      $this->db->select('patrocinio.*, marca.nombre_mar as nombre_mar');
      $this->db->from('patrocinio');
      $this->db->join('revista', 'patrocinio.fk_id_rev = revista.id_rev', 'left');
      $this->db->join('marca', 'patrocinio.fk_id_mar = marca.id_mar', 'left');
      $query = $this->db->get();

      if($query->num_rows()>0){
        return $query->result();
      } else {
        return false;
      }
    }

    public function consultarMarca()
    {
        $this->db->select('id_mar, nombre_mar');
        $query = $this->db->get("marca");
        return $query->result();
    }

    public function consultarRevista()
    {
        $this->db->select('id_rev, nombre_rev');
        $query = $this->db->get("revista");
        return $query->result();
    }


    // Eliminar un patrocinio por su ID
    function eliminar($id){
        $this->db->where("id_pat",$id);
        return $this->db->delete("patrocinio");
    }

    // Obtener los datos de un patrocinio por su ID
    function obtenerPorId($id){
        $this->db->where("id_pat",$id);
        $patrocinios=$this->db->get("patrocinio");
        if($patrocinios->num_rows()>0){
            return $patrocinios->row();
        } else {
            return false;
        }
    }

    // Actualizar los datos de un patrocinio por su ID
    function actualizar($id,$datos){
        $this->db->where("id_pat",$id);
        return $this->db->update("patrocinio",$datos);
    }

}

?>
