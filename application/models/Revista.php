<?php
/**
 *
 */
class Revista extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    // Insertar nuevos datos de revista
    function insertar($datos){
        $respuesta=$this->db->insert("revista",$datos);
        return $respuesta;
    }

    // Consultar todas las revistas con sus autores y directores
    function consultarTodos(){
      $this->db->select('revista.*, autores.nombres_aut as nombre_autor');
      $this->db->select('revista.*, director.nombre_dir as nombre_dir');
      $this->db->from('revista');
      $this->db->join('autores', 'revista.fk_id_aut = autores.id_aut', 'left');
      $this->db->join('director', 'revista.fk_id_dir = director.id_dir', 'left');
      $query = $this->db->get();

      if($query->num_rows()>0){
        return $query->result();
      } else {
        return false;
      }
    }

    public function consultarAutores()
    {
        $this->db->select('id_aut, apellidos_aut');
        $this->db->select('id_aut, nombres_aut');
        $query = $this->db->get("autores");
        return $query->result();
    }

    public function consultarDirectores()
    {
        $this->db->select('id_dir, apellido_dir');
        $this->db->select('id_dir, nombre_dir');
        $query = $this->db->get("director");
        return $query->result();
    }

    // Eliminar una revista por su ID
    function eliminar($id){
        $this->db->where("id_rev",$id);
        return $this->db->delete("revista");
    }

    // Obtener los datos de una revista por su ID
    function obtenerPorId($id){
        $this->db->where("id_rev",$id);
        $revistas=$this->db->get("revista");
        if($revistas->num_rows()>0){
            return $revistas->row();
        } else {
            return false;
        }
    }

    // Actualizar los datos de una revista por su ID
    function actualizarRevista($id,$datos){
        $this->db->where("id_rev",$id);
        return $this->db->update("revista",$datos);
    }

}

?>
