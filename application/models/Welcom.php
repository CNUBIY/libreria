<?php
class Welcom extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //consulta de datos
    function consultarTodos() {
        $this->db->select('articulo.*, revista.nombre_rev as nombre_rev, autores.apellidos_aut as apellidos_aut, autores.nombres_aut as nombres_aut, director.nombre_dir as nombre_dir, director.apellido_dir as apellido_dir');
        $this->db->from('articulo');
        $this->db->join('grupo_trabajo', 'articulo.id_art = grupo_trabajo.fk_id_art', 'left');
        $this->db->join('autores', 'grupo_trabajo.fk_id_aut = autores.id_aut', 'left');
        $this->db->join('revista', 'revista.fk_id_aut = autores.id_aut', 'left');
        $this->db->join('director', 'revista.fk_id_dir = director.id_dir', 'left');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function obtenerPorId($id_art) {
        $this->db->select('articulo.*, revista.nombre_rev as nombre_rev, articulo.fecha_art, articulo.titulo_art, articulo.volumen_art, articulo.numero_art, articulo.url_art, director.nombre_dir as nombre_director, director.apellido_dir as apellido_director');
        $this->db->from('articulo');
        $this->db->join('grupo_trabajo', 'articulo.id_art = grupo_trabajo.fk_id_art', 'left');
        $this->db->join('autores', 'grupo_trabajo.fk_id_aut = autores.id_aut', 'left');
        $this->db->join('revista', 'autores.id_aut = revista.fk_id_aut', 'left');
        $this->db->join('director', 'revista.fk_id_dir = director.id_dir', 'left');
        $this->db->where('articulo.id_art', $id_art);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->row();

            // Obtener los nombres completos de los autores relacionados con este artículo
            $this->db->select('CONCAT(autores.nombres_aut, " ", autores.apellidos_aut) as nombre_aut');
            $this->db->from('grupo_trabajo');
            $this->db->join('autores', 'grupo_trabajo.fk_id_aut = autores.id_aut', 'left');
            $this->db->where('grupo_trabajo.fk_id_art', $id_art);
            $autores_query = $this->db->get();

            // Almacenar los nombres completos de los autores en un array
            $autores = array();
            foreach ($autores_query->result() as $row) {
                $autores[] = $row->nombre_aut;
            }

            // Agregar los nombres de los autores al resultado
            $result->autores = $autores;

            // Obtener los nombres de las marcas relacionadas con este artículo
            $this->db->select('marca.logo_mar as logo_mar');
            $this->db->from('marca');
            $this->db->join('patrocinio', 'marca.id_mar = patrocinio.fk_id_mar', 'left');
            $this->db->join('revista', 'patrocinio.fk_id_rev = revista.id_rev', 'left');
            $this->db->join('articulo', 'revista.id_rev = articulo.id_art', 'left');
            $this->db->where('articulo.id_art', $id_art);
            $marcas_query = $this->db->get();

            // Almacenar los nombres de las marcas en un array
            $marcas = array();
            foreach ($marcas_query->result() as $row) {
                $marcas[] = $row->logo_mar;
            }

            // Agregar los nombres de las marcas al resultado
            $result->marcas = $marcas;

            return $result;
        } else {
            return false;
        }
    }

}

 ?>
