<!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Agregar</h3>
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                          <li class="breadcrumb-item"><a href="<?php echo site_url('articulos/index'); ?>">Articulos</a></li>
                          <li class="breadcrumb-item active">Agregar Articulo</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <i class="fa-solid fa-list fa-10x " aria-hidden="true"></i>
                                    <h4 class="card-title m-t-10">Articulos</h4>
                                    <h6 class="card-subtitle">Registra el nuevo articulo</h6>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <form action="<?php echo site_url('articulos/guardarArticulo'); ?>" id="frm_nuevo_articulo"  enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                                  <script type="text/javascript">
                                  $("#id_provincia").select2({
                                    maximumSelectionLength: 2
                                  });
                                  </script>
                                    <div class="form-group">
                                        <label class="col-md-12">Titulo</label>
                                        <div class="col-md-12">
                                            <input required type="text" name="titulo_art" id="titulo_art" placeholder="Ingrese el titulo" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <script>
                                    document.getElementById('titulo_art').onkeypress = function(e) {
                                      var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                      var charTyped = String.fromCharCode(charCode);
                                      var regex = /^[A-Za-záéíóúÁÉÍÓÚ\s]+$/;
                                      if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                        e.preventDefault();
                                      }
                                    };
                                    </script>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Fecha</label>
                                        <div class="col-md-12">
                                            <input required type="text" name="fecha_art" id="fecha_art" placeholder="Ingrese la fecha" class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <script>
                                        document.getElementById('fecha_art').onkeypress = function(e) {
                                            var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                            var charTyped = String.fromCharCode(charCode);
                                            var regex = /^[0-9\/-]+$/;
                                            if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                                e.preventDefault();
                                            }
                                        };
                                    </script>


                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Volumen</label>
                                        <div class="col-md-12">
                                            <input required type="text" name="volumen_art" id="volumen_art" placeholder="Ingrese el volumen del articulo" class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <script>
                                        document.getElementById('volumen_art').onkeypress = function(e) {
                                            var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                            if (charCode < 48 || charCode > 57) {
                                                e.preventDefault();
                                            }
                                        };
                                    </script>

                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Numero</label>
                                        <div class="col-md-12">
                                            <input required type="text" name="numero_art" id="numero_art" placeholder="Ingrese el volumen del articulo" class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <script>
                                        document.getElementById('numero_art').onkeypress = function(e) {
                                            var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                            if (charCode < 48 || charCode > 57) {
                                                e.preventDefault();
                                            }
                                        };
                                    </script>

                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">URL</label>
                                        <div class="col-md-12">
                                            <input required type="text" name="url_art" id="url_art" placeholder="Ingrese el volumen del articulo" class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                   </div>
                                    <script>
                                        document.getElementById('url_art').onkeypress = function(e) {
                                            var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                            var charTyped = String.fromCharCode(charCode);
                                            var regex = /^[a-zA-Z0-9-_.:/]+$/;
                                            if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                                e.preventDefault();
                                            }
                                        };
                                    </script>


                                    <br>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <center>
                                              <button type="submit" name="button" class="btn btn-outline-info">
                                                <i class="fa fa-save"></i>
                                                Guardar</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="<?php echo site_url('articulos/index'); ?>" class="btn btn-outline-danger">
                                                  <i class="fa fa-times"></i>
                                                  Cancelar</a>
                                          </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>

            <script type="text/javascript">
        $("#frm_nuevo_articulo").validate({
          rules:{
            "id_art":{
              required:true
            },
            "titulo_art":{
              required:true,
            },
            "fecha_art":{
              required:true
            },
            "volumen_art":{
              required:true
            },
            "numero_art":{
              required :true
            },
            "url_art":{
              required:true
            }
          },
          messages:{
            "id_art":{
              required:"ID articulo"
            },
            "titulo_art":{
              required:"Ingrese el titulo",
            },
            "fecha_art":{
              required:"Ingrese la fecha"
            },
            "volumen_art":{
              required:"Ingrese el volumen"
            },
            "numero_art":{
              required :"Ingrese el numero"
            },
            "url_art":{
              required:"Ingrese la URL"
            }
          }
        });
      </script>
