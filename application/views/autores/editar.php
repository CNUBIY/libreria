
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Editar</h3>
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                          <li class="breadcrumb-item"><a href="<?php echo site_url('autores/index'); ?>">Autores</a></li>
                          <li class="breadcrumb-item active">Editar Autores</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <img src="<?php echo base_url('assets/images/img21.png'); ?>" class="img-circle" width="80%" />
                                    <h4 class="card-title m-t-10">Autores Solidario</h4>
                                    <h6 class="card-subtitle">Edita el autor</h6>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <form action="<?php echo site_url('autores/actualizarAutor'); ?>" id="frm_nuevo_autor"  enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                                  <input type="hidden" name="id_aut" id="id_aut" value="<?php echo $autorEditar->id_aut; ?>">
                                    <div class="form-group">
                                        <label class="col-md-12">Apellidos</label>
                                        <div class="col-md-12">
                                            <input required type="text" value="<?php echo $autorEditar->apellidos_aut; ?>" name="apellidos_aut" id="apellidos_aut" placeholder="Ingrese el apellido del autor" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <script>
                                    document.getElementById('apellidos_aut').onkeypress = function(e) {
                                      var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                      var charTyped = String.fromCharCode(charCode);
                                      var regex = /^[A-Za-záéíóúÁÉÍÓÚ\s]+$/;
                                      if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                        e.preventDefault();
                                      }
                                    };
                                    </script>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Nombres</label>
                                        <div class="col-md-12">
                                            <input required type="text" value="<?php echo $autorEditar->nombres_aut; ?>" name="nombres_aut" id="nombres_aut" placeholder="Ingrese el nombre del autor" class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <script>
                                    document.getElementById('nombres_aut').onkeypress = function(e) {
                                      var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                      var charTyped = String.fromCharCode(charCode);
                                      var regex = /^[A-Za-záéíóúÁÉÍÓÚ\s]+$/;
                                      if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                        e.preventDefault();
                                      }
                                    };
                                    </script>
                                    <br>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <center>
                                              <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
                                              <a href="<?php echo site_url('autores/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark"></i> &nbsp Cancelar</a>
                                          </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <script type="text/javascript">
        $("#frm_nuevo_autor").validate({
          rules:{
            "id_art":{
              required:true
            },
            "titulo":{
              required:true,
            },
            "fecha_art":{
              required:true
            },
            "volumen_art":{
              required:true
            },
            "numero_art":{
              required :true
            },
            "url_art":{
              required:true
            }
          },
          messages:{
            "id_aut":{
              required:"Id de autor"
            },
            "apellidos_aut":{
              required:"Ingrese el apellido",
            },
            "nombres_aut":{
              required:"Ingrese el nombre"
            }
          }
        });
      </script>
