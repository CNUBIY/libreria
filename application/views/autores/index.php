
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Autores</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                            <li class="breadcrumb-item active">Autores</li>
                        </ol>
                    </div>
                    <div class="col-md-6 col-4 align-self-center">
                        <a href="<?php echo site_url('autores/nuevo') ?>" class="btn pull-right hidden-sm-down btn-success">Agregar Autor</a>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Autores</h4>
                                <h6 class="card-subtitle">UTC -<code>Banco Solidario</code></h6>
                                <div class="table-responsive">
                                    <?php if ($listadoAutores): ?>
                                    <table class="table" id="tbl_2">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Apellidos</th>
                                                <th>Nombres</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php foreach ($listadoAutores as $autores): ?>
                                            <tr>
                                              <td><?php echo $autores->id_aut; ?></td>
                                              <td><?php echo $autores->apellidos_aut; ?></td>
                                              <td><?php echo $autores->nombres_aut; ?></td>
                                              <td>
                                                <a href="<?php echo site_url('autores/editar/').$autores->id_aut; ?>" class="btn btn-warning" title="Editar"><i class="fa fa-pen"></i></a>
                                                <a class="btn btn-danger delete-btn" href="<?php echo site_url('autores/borrar/').$autores->id_aut; ?>" title="Eliminar" data-id_art="<?php echo $autores->id_aut; ?>"><i class="fa fa-trash"></i></a>
                                              </td>
                                            </tr>
                                          <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                  <?php else: ?>
                                    <div class="alert alert-danger">
                                      No se encontró autores registrados
                                    </div>
                                  <?php endif; ?>
                                </div><br>
                                <button id="toggleChartButton" onclick="toggleChart()" class="btn btn-outline-info">Mostrar/ocultar gráfico</button>
                                <div id="chartContainer" style="width: 50%; margin: auto; border: 1px solid black; padding: 10px; display: none;">
                                  <canvas id="myChart"></canvas>
                                  <canvas id="myPieChart"></canvas>
                                </div><br>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <script type="text/javascript">
                    $(document).ready(function() {
                        $('#tbl_2').DataTable( {
                            dom: 'Bfrtip',
                            buttons: [
                                {
                                    extend: 'pdfHtml5',
                                    text: '<i class="fa-solid fa-file-pdf"></i> Exportar a PDF',
                                    className: 'btn btn-outline-info',
                                    messageTop: 'REPORTE DE AUTORES ',
                                    title:'INFORMACIÓN REPORTES'
                                },
                                {
                                    extend: 'print',
                                    text: '<i class="fa-solid fa-print"></i> Imprimir',
                                    className: 'btn btn-outline-info',
                                    messageTop: 'REPORTE DE AUTORES ',
                                    title:'INFORMACIÓN REPORTES'
                                },
                                {
                                    extend: 'csv',
                                    text: '<i class="fa-solid fa-file-csv"></i> Exportar a CSV',
                                    className: 'btn btn-outline-info',
                                    messageTop: 'REPORTE DE AUTORES ',
                                    title:'INFORMACIÓN REPORTES'
                                }
                            ],
                            language: {
                                url: "https://cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json"
                            }
                        } );
                    } );
                </script>

    <script>
    $(document).ready(function() {
        $('.delete-btn').click(function(event) {
            // Evitar el comportamiento predeterminado del enlace
            event.preventDefault();

            var id = $(this).data('id_art');
            Swal.fire({
                title: '¿Estás seguro de que quieres eliminar este registro?',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sí',
                denyButtonText: 'No',
                customClass: {
                    actions: 'my-actions',
                    cancelButton: 'order-1 right-gap',
                    confirmButton: 'order-2',
                    denyButton: 'order-3',
                },
            }).then((result) => {
                if (result.isConfirmed) {
                    // Realizar la acción de eliminación
                    // Por ejemplo, redireccionar a una URL que maneje la eliminación
                    window.location.href = "<?php echo site_url('autores/borrar/'); ?>" + id;
                    // No redirigir aquí, dejar que el servidor maneje la redirección después de eliminar
                } else if (result.isDenied) {
                    // No hacer nada si el usuario cancela la eliminación
                }
            });
        });
    });

</script>
