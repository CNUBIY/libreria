
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Agregar</h3>
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                          <li class="breadcrumb-item"><a href="<?php echo site_url('autores/index'); ?>">Autores</a></li>
                          <li class="breadcrumb-item active">Agregar Autor</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <i class="fa-solid fa-user m-r-10 fa-10x" aria-hidden="true"></i>
                                    <h4 class="card-title m-t-10">Autores</h4>
                                    <h6 class="card-subtitle">Registra el nuevo autor</h6>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <form action="<?php echo site_url('autores/guardarAutor'); ?>" id="frm_nuevo_autor"  enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                                  <script type="text/javascript">
                                  $("#id_provincia").select2({
                                    maximumSelectionLength: 2
                                  });
                                  </script>
                                    <div class="form-group">
                                        <label class="col-md-12">Apellidos</label>
                                        <div class="col-md-12">
                                            <input required type="text" name="apellidos_aut" id="apellidos_aut" placeholder="Ingrese el apellido" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <script>
                                    document.getElementById('apellidos_aut').onkeypress = function(e) {
                                      var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                      var charTyped = String.fromCharCode(charCode);
                                      var regex = /^[A-Za-záéíóúÁÉÍÓÚ\s]+$/;
                                      if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                        e.preventDefault();
                                      }
                                    };
                                    </script>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Nombres:</label>
                                        <div class="col-md-12">
                                            <input required type="text" name="nombres_aut" id="nombres_aut" placeholder="Ingrese el nombre" class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <script>
                                    document.getElementById('nombres_aut').onkeypress = function(e) {
                                      var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                      var charTyped = String.fromCharCode(charCode);
                                      var regex = /^[A-Za-záéíóúÁÉÍÓÚ\s]+$/;
                                      if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                        e.preventDefault();
                                      }
                                    };
                                    </script>
                                    <br>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <center>
                                              <button type="submit" name="button" class="btn btn-outline-info">
                                                <i class="fa fa-save"></i>
                                                Guardar</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="<?php echo site_url('autores/index'); ?>" class="btn btn-outline-danger">
                                                  <i class="fa fa-times"></i>
                                                  Cancelar</a>
                                          </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>

            <script type="text/javascript">
        $("#frm_nuevo_autor").validate({
          rules:{
            "id_aut":{
              required:true
            },
            "apellidos_aut":{
              required:true,
            },
            "nombres_aut":{
              required:true
            }
          },
          messages:{
            "id_aut":{
              required:"ID autor"
            },
            "apellidos_aut":{
              required:"Ingrese el apellido del autor",
            },
            "nombres_aut":{
              required:"Ingrese el nombre del autor"
            }
          }
        });
      </script>
