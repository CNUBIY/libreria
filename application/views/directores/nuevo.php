<!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Agregar</h3>
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                          <li class="breadcrumb-item"><a href="<?php echo site_url('directores/index'); ?>">Directores</a></li>
                          <li class="breadcrumb-item active">Agregar Articulo</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <i class="fa-solid fa-clipboard m-r-10 fa-10x"></i>
                                    <h4 class="card-title m-t-10">Directores</h4>
                                    <h6 class="card-subtitle">Registra el nuevo Director</h6>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <form action="<?php echo site_url('directores/guardarDirector'); ?>" id="frm_nuevo_director"  enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                                  <script type="text/javascript">
                                  $("#id_provincia").select2({
                                    maximumSelectionLength: 2
                                  });
                                  </script>
                                    <div class="form-group">
                                        <label class="col-md-12">Nombre</label>
                                        <div class="col-md-12">
                                            <input required type="text" name="nombre_dir" id="nombre_dir" placeholder="Ingrese el nombre" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <script>
                                    document.getElementById('nombre_dir').onkeypress = function(e) {
                                      var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                      var charTyped = String.fromCharCode(charCode);
                                      var regex = /^[A-Za-záéíóúÁÉÍÓÚ\s]+$/;
                                      if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                        e.preventDefault();
                                      }
                                    };
                                    </script>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Apellido</label>
                                        <div class="col-md-12">
                                            <input required type="text" name="apellido_dir" id="apellido_dir" placeholder="Ingrese el apellido" class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <script>
                                    document.getElementById('apellido_dir').onkeypress = function(e) {
                                      var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                      var charTyped = String.fromCharCode(charCode);
                                      var regex = /^[A-Za-záéíóúÁÉÍÓÚ\s]+$/;
                                      if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                        e.preventDefault();
                                      }
                                    };
                                    </script>


                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Correo</label>
                                        <div class="col-md-12">
                                            <input required type="email" name="correo_dir" id="correo_dir" placeholder="Ingrese el correo del director" class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <script>
                                    document.getElementById('correo_dir').addEventListener('input', function() {
                                        var correo = document.getElementById('correo_dir').value;
                                        var re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
                                        if (!re.test(correo)) {
                                            document.getElementById('correo_dir').setCustomValidity('Ingrese un correo electrónico válido');
                                        } else {
                                            document.getElementById('correo_dir').setCustomValidity('');
                                        }
                                    });
                                </script>





                                    <br>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <center>
                                              <button type="submit" name="button" class="btn btn-outline-info">
                                                <i class="fa fa-save"></i>
                                                Guardar</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="<?php echo site_url('directores/index'); ?>" class="btn btn-outline-danger">
                                                  <i class="fa fa-times"></i>
                                                  Cancelar</a>
                                          </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>

            <script type="text/javascript">
        $("#frm_nuevo_director").validate({
          rules:{
            "id_dir":{
              required:true
            },
            "nombre_dir":{
              required:true,
            },
            "apellido_dir":{
              required:true
            },
            "correo_dir":{
              required:true
            }
          },
          messages:{
            "id_dir":{
              required:"Id del director"
            },
            "nombre_dir":{
              required:"Ingrese el nombre",
            },
            "apellido_dir":{
              required:"Ingrese el apellido"
            },
            "correo_dir":{
              required:"Ingre el correo"
            }
          }
        });
      </script>
