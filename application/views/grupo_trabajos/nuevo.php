
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Agregar</h3>
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                          <li class="breadcrumb-item"><a href="<?php echo site_url('grupo_trabajos/index'); ?>">Grupo_trabajo</a></li>
                          <li class="breadcrumb-item active">Agregar Grupo_trabajo</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <i class="fa-solid fa-users m-r-10 fa-10x" aria-hidden="true"></i>
                                    <h4 class="card-title m-t-10">Grupo_trabajo</h4>
                                    <h6 class="card-subtitle">Registra el nuevo grupo de trabajo</h6>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <form action="<?php echo site_url('grupo_trabajos/guardarGrupo_trabajo'); ?>" id="frm_nuevo_grupo_trabajo"  enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                                  <div class="form-group">
                                      <label class="col-md-12">Autores</label>
                                      <select required name="fk_id_aut"  id="fk_id_aut" class="form-control form-control-line">
                                        <option value="">Escoge el id autor</option>
                                        <?php foreach ($listadoAutores as $autores): ?>
                                          <option value="<?php echo $autores->id_aut; ?>">
                                            <?php echo $autores->nombres_aut . ' ' .$autores->apellidos_aut; ?>
                                          </option>
                                        <?php endforeach; ?>
                                      </select>
                                  </div>
                                  <script type="text/javascript">
                                  $("#fk_id_aut").select2({
                                    maximumSelectionLength: 2
                                  });
                                  </script>
                                    <br>
                                    <div class="form-group">
                                        <label class="col-md-12">Articulos</label>
                                        <select required name="fk_id_art"  id="fk_id_art" class="form-control form-control-line">
                                          <option value="">Escoge el id autor</option>
                                          <?php foreach ($listadoArticulos as $articulos): ?>
                                            <option value="<?php echo $articulos->id_art; ?>"><?php echo $articulos->titulo_art; ?></option>
                                          <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <script type="text/javascript">
                                    $("#fk_id_art").select2({
                                      maximumSelectionLength: 2
                                    });
                                    </script>
                                      <br>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <center>
                                              <button type="submit" name="button" class="btn btn-outline-info">
                                                <i class="fa fa-save"></i>
                                                Guardar</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="<?php echo site_url('grupo_trabajos/index'); ?>" class="btn btn-outline-danger">
                                                  <i class="fa fa-times"></i>
                                                  Cancelar</a>
                                          </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <script type="text/javascript">
        $("#frm_nuevo_grupo_trabajo").validate({
          rules:{
            "fk_id_aut":{
              required:true
            },
            "fk_id_art":{
              required:true
            }
          },
          messages:{
            "fk_id_aut":{
              required:"Debe seleccionar el id del autor"
            },
            "fk_id_art":{
              required:"Debe seleccionar el id de articulo"
            }
          }
        });
      </script>
