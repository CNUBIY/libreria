<!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Agregar</h3>
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                          <li class="breadcrumb-item"><a href="<?php echo site_url('marcas/index'); ?>">Marcas</a></li>
                          <li class="breadcrumb-item active">Agregar Marcas</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <i class="fa-brands fa-apple m-r-10 fa-10x" aria-hidden="true"></i>
                                    <h4 class="card-title m-t-10">Marcas</h4>
                                    <h6 class="card-subtitle">Registra la nueva marca</h6>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <form id="frm_nuevo_marca" action="<?php echo site_url('marcas/guardarMarca'); ?>"  enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                                    <div class="form-group">
                                        <label class="col-md-12">Nombre</label>
                                        <div class="col-md-12">
                                            <input required type="text" name="nombre_mar" id="nombre_mar" placeholder="Ingrese el nombre de la marca" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <script>
                                    document.getElementById('nombre_mar').onkeypress = function(e) {
                                      var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                      var charTyped = String.fromCharCode(charCode);
                                      var regex = /^[A-Za-záéíóúÁÉÍÓÚ\s]+$/;
                                      if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                        e.preventDefault();
                                      }
                                    };
                                    </script>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Logo de la Marca</label>
                                        <div class="col-md-12">
                                            <input required type="file" name="logo_mar" id="logo_mar" placeholder="Ingrese el logo de la entidad" class="form-control form-control-line" accept="image/*">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <center>
                                              <button type="submit" name="button" class="btn btn-outline-info">
                                                <i class="fa fa-save"></i>
                                                Guardar</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="<?php echo site_url('marcas/index'); ?>" class="btn btn-outline-danger">
                                                  <i class="fa fa-times"></i>
                                                  Cancelar</a>
                                          </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>

            <script type="text/javascript">
$("#frm_nuevo_marca").validate({
  rules:{
    "id_mar":{
      required:true
    },
    "nombre_mar":{
      required:true,
    },
    "logo_mar":{
      required:true
    } // Agrega una coma aquí
  },
  messages:{
    "id_mar":{
      required:"Debe seleccionar la marca"
    },
    "nombre_mar":{
      required:"Ingrese la marca",
    },
    "logo_mar":{
      required:"Seleccione el logo de la marca"
    },
  }
});




</script>
