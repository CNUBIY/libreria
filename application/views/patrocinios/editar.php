
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Editar</h3>
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                          <li class="breadcrumb-item"><a href="<?php echo site_url('patrocinios/index'); ?>">Patrocinio</a></li>
                          <li class="breadcrumb-item active">Editar Patrocinio</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <img src="<?php echo base_url('assets/images/img21.png'); ?>" class="img-circle" width="80%" />
                                    <h4 class="card-title m-t-10">Patrocinios</h4>
                                    <h6 class="card-subtitle">Edita el patrocinio</h6>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <form action="<?php echo site_url('patrocinios/actualizarPatrocinio'); ?>" id="frm_nuevo_patrocinio"  enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                                  <input type="hidden" name="id_pat" id="id_pat" value="<?php echo $patrocinioEditar->id_pat; ?>">
                                  <div class="form-group">
                                      <label class="col-md-12">Revista</label>
                                      <select required name="fk_id_rev" id="fk_id_rev" class="form-control form-control-line">
                                          <option value="">Escoge la revista</option>
                                          <?php foreach ($listadoRevistas as $revista): ?>
                                              <option value="<?php echo $revista->id_rev; ?>" <?php echo ($revista->id_rev == $patrocinioEditar->fk_id_rev) ? 'selected' : ''; ?>><?php echo $revista->nombre_rev; ?></option>
                                          <?php endforeach; ?>
                                      </select>
                                  </div>


                                  <script type="text/javascript">
                                  $("#fk_id_rev").select2({
                                    maximumSelectionLength: 2
                                  });
                                  </script>

                                  <br>
                                  <div class="form-group">
                                      <label class="col-md-12">Marca</label>
                                      <select required name="fk_id_mar" id="fk_id_mar" class="form-control form-control-line">
                                          <option value="">Escoge la marca</option>
                                          <?php foreach ($listadoMarca as $marca): ?>
                                              <option value="<?php echo $marca->id_mar; ?>" <?php echo ($marca->id_mar == $patrocinioEditar->fk_id_mar) ? 'selected' : ''; ?>><?php echo $marca->nombre_mar; ?></option>
                                          <?php endforeach; ?>
                                      </select>
                                  </div>

                                  <script type="text/javascript">
                                  $("#fk_id_mar").select2({
                                    maximumSelectionLength: 2
                                  });
                                  </script>

                                    <br>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <center>
                                              <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
                                              <a href="<?php echo site_url('patrocinios/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark"></i> &nbsp Cancelar</a>
                                          </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>

            <script type="text/javascript">
        $("#frm_nuevo_patrocinio").validate({
          rules:{
            "fk_id_rev":{
              required:true
            },
            "fk_id_mar":{
              required:true
            }
          },
          messages:{
            "fk_id_rev":{
              required:"Debe seleccionar el id de revista"
            },
            "fk_id_mar":{
              required:"Debe seleccionar el id de la marca"
            }
          }
        });
      </script>
