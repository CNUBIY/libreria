<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Agregar</h3>
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo site_url('patrocinios/index'); ?>">Patrocinios</a></li>
                  <li class="breadcrumb-item active">Agregar Patrocinios</li>
                </ol>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <div class="card-block">
                        <center class="m-t-30"> <i class="fa-solid fa-money-bill m-r-10 fa-10x"></i>
                            <h4 class="card-title m-t-10">Patrocinios</h4>
                            <h6 class="card-subtitle">Registra el nuevo patrocinador</h6>
                        </center>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <div class="card-block">
                        <form action="<?php echo site_url('patrocinios/guardarPatrocinio'); ?>" id="frm_nuevo_patrocinio"  enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                          <div class="form-group">
                              <label class="col-md-12">Revistas</label>
                              <select required name="fk_id_rev"  id="fk_id_rev" class="form-control form-control-line">
                                  <option value="">Escoge la revista</option>
                                  <?php foreach ($listadoRevistas as $revista): ?>
                                      <option value="<?php echo $revista->id_rev; ?>"><?php echo $revista->nombre_rev; ?></option>
                                  <?php endforeach; ?>
                              </select>
                          </div>

                          <script type="text/javascript">
                          $("#fk_id_rev").select2({
                            maximumSelectionLength: 2
                          });
                          </script>

                            <br>
                            <div class="form-group">
                                <label class="col-md-12">Marcas</label>
                                <select required name="fk_id_mar"  id="fk_id_mar" class="form-control form-control-line">
                                  <option value="">Escoge la marca</option>
                                  <?php foreach ($listadoMarca as $marca): ?>
                                    <option value="<?php echo $marca->id_mar; ?>"><?php echo $marca->nombre_mar; ?></option>
                                  <?php endforeach; ?>
                                </select>
                            </div>
                            <script type="text/javascript">
                            $("#fk_id_mar").select2({
                              maximumSelectionLength: 2
                            });
                            </script>
                              <br>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <center>
                                      <button type="submit" name="button" class="btn btn-outline-info">
                                        <i class="fa fa-save"></i>
                                        Guardar</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="<?php echo site_url('patrocinios/index'); ?>" class="btn btn-outline-danger">
                                          <i class="fa fa-times"></i>
                                          Cancelar</a>
                                  </center>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <script type="text/javascript">
$("#frm_nuevo_patrocinio").validate({
  rules:{
    "fk_id_mar":{
      required:true
    },
    "fk_id_rev":{
      required:true
    }
  },
  messages:{
    "fk_id_mar":{
      required:"Debe seleccionar la marca"
    },
    "fk_id_rev":{
      required:"Debe seleccionar la revista"
    }
  }
});
</script>
