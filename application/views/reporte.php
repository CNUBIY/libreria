<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

 // Obtén la ruta completa hacia tcpdf.php utilizando FCPATH
 $file_path = FCPATH . 'assets/tcpdf/tcpdf.php';
 $logo_path = FCPATH . 'assets/images/logo_utc.jpg';

 // Incluye el archivo tcpdf.php
 require_once($file_path);

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Carlos Colcha');
$pdf->SetTitle('Universidad Técnica de Cotopaxi');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
// set default header data
$pdf->SetHeaderData($logo_path,0, 'Universidad Técnica de Cotopaxi', 'Por Carlos Colcha', array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
$nombre_revista = $listadoArticulos->nombre_rev;
$fecha_articulo = $listadoArticulos->fecha_art;
$logo_url = base_url('assets/images/logo_utc.jpg');
$titulo_articulo = $listadoArticulos->titulo_art;
$volumen_articulo = $listadoArticulos->volumen_art;
$numero_articulo = $listadoArticulos->numero_art;
$id_articulo = $listadoArticulos->id_art;
$url_articulo = $listadoArticulos->url_art;
$nombre_director = $listadoArticulos->nombre_director;
$apellido_director = $listadoArticulos->apellido_director;

$html = <<<EOD
<div style="text-align: right;">
    <img src="$logo_url" alt="Logo UTC" style="width: 50px; height: auto; vertical-align: middle; margin-left: 10px;">
    <h1 style="font-size: 22px; font-weight: bold; color: #00008B; display: inline-block; margin-bottom: 0;">Revista $nombre_revista!</h1>
</div>
<p style="text-align: right; font-family: Arial, sans-serif; font-size: 12px; line-height: 1.5; margin-bottom: 5px; margin-top: 5px;">$fecha_articulo</p>
<p style="font-family: Arial, sans-serif; font-size: 14px; line-height: 1.5; margin-bottom: 5px;"><b>
EOD;

// Verificar si existen autores
if (isset($listadoArticulos->autores) && is_array($listadoArticulos->autores) && count($listadoArticulos->autores) > 0) {
    // Iterar sobre cada autor y agregarlo al HTML
    foreach ($listadoArticulos->autores as $autor) {
        $html .= "<p style='font-family: Arial, sans-serif; font-size: 14px; line-height: 1.5; margin-bottom: 5px;'>$autor</p>";
    }
} else {
    // Si no hay autores, mostrar un mensaje indicando que no hay autores disponibles
    $html .= "<p style='font-family: Arial, sans-serif; font-size: 14px; line-height: 1.5; margin-bottom: 5px;'>No hay autores disponibles</p>";
}

$html .= <<<EOD
</b></p>
<p style="font-family: Arial, sans-serif; font-size: 13px; line-height: 1.5; margin-bottom: 5px;"><b>Presentes</b></p> <br><!-- Agregamos esta línea para indicar que se listarán los autores -->
<p style="font-family: Arial, sans-serif; font-size: 12px; line-height: 1.5; margin-bottom: 20px;">Reciban un cordial saludo y sirva este medio para informarle que, una vez realizado el proceso de arbitraje, el Comité Editorial de la revista <b>$nombre_revista</b> ha decidido publicar su artículo titulado:</p>
<p style="text-align:center; font-family: Arial, sans-serif; font-size: 14px; line-height: 1.5; margin-bottom: 20px;"><b>"$titulo_articulo"</b></p> <!-- Agregar el título del artículo aquí -->
<p style="font-family: Arial, sans-serif; font-size: 12px; line-height: 1.5; margin-bottom: 20px;">Mismo que cumple con los lineamientos estipulados para la publicación.</p>
<p style="font-family: Arial, sans-serif; font-size: 12px; line-height: 1.5; margin-bottom: 20px;">Su artículo es presentado en forma digital y formato PDF que se incluye en el volumen "$volumen_articulo", número "$numero_articulo" de nuestra Revista con ISSN: "$id_articulo" y dirección electrónica: <a href="$url_articulo">$url_articulo</a></p>
<p style="font-family: Arial, sans-serif; font-size: 12px; line-height: 1.5; margin-bottom: 20px;">El comité de la revista <b>$nombre_revista</b> agradece su participación y le invita a seguir colaborando con nosotros, ya que es grato contar con tan valiosas aportaciones</p>
<p style="text-align:center; font-family: Arial, sans-serif; font-size: 12px; line-height: 1.5; margin-bottom: 20px;"><b>Sin otro particular, le saluda <br> ATENTAMENTE</b></p>
<p></p>
<p></p>
<p></p>

<p style="text-align:center; font-family: Arial, sans-serif; font-size: 14px; line-height: 1.5; margin-bottom: 20px;">$nombre_director $apellido_director<br>Director Editorial</p>
<p style="text-align:center; font-family: Arial, sans-serif; font-size: 12px; line-height: 1.5; margin-bottom: 20px;">Marcas asociadas al artículo:</p>
<div style="text-align:center;">
EOD;

foreach ($listadoArticulos->marcas as $marca) {
    // Agregar el nombre de la marca en un párrafo
    // Construir la URL completa de la imagen
    $imagen_url = base_url('uploads/marcas/') . $marca;

    // Imprimir la URL para verificar
    //echo "URL de la imagen: $imagen_url <br>";

    // Agregar la imagen de la marca en un párrafo
    $html .= "<img src='$imagen_url' alt='Logo de $marca'>";
}


$html .= "</div>";






// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
