
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Editar</h3>
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                          <li class="breadcrumb-item"><a href="<?php echo site_url('revistas/index'); ?>">Revista</a></li>
                          <li class="breadcrumb-item active">Editar Revista</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <img src="<?php echo base_url('assets/images/img21.png'); ?>" class="img-circle" width="80%" />
                                    <h4 class="card-title m-t-10">Revistas</h4>
                                    <h6 class="card-subtitle">Edita la revista</h6>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <form action="<?php echo site_url('revistas/actualizarRevista'); ?>" id="frm_nuevo_revista"  enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                                  <input type="hidden" name="id_rev" id="id_rev" value="<?php echo $revistaEditar->id_rev; ?>">
                                  <div class="form-group">
                                      <label class="col-md-12">Autores</label>
                                      <select required name="fk_id_aut" id="fk_id_aut" class="form-control form-control-line">
                                          <option value="">Escoge el id autor</option>
                                          <?php foreach ($listadoAutores as $autores): ?>
                                              <?php $selected = ($autores->fk_id_aut == $revistaEditar->fk_id_aut) ? 'selected' : ''; ?>
                                              <option value="<?php echo $autores->fk_id_aut; ?>" <?php echo $selected; ?>><?php echo $autores->fk_id_aut; ?></option>
                                          <?php endforeach; ?>
                                      </select>
                                  </div>

                                  <script type="text/javascript">
                                  $("#fk_id_aut").select2({
                                    maximumSelectionLength: 2
                                  });
                                  </script>

                                  <br>
                                  <div class="form-group">
                                    <label class="col-md-12">Directores</label>
                                    <select required name="fk_id_dir" id="fk_id_dir" class="form-control form-control-line">
                                        <option value="">Escoge el id de director</option>
                                        <?php foreach ($listadoDirectores as $directores): ?>
                                            <?php $selected = ($directores->fk_id_dir == $revistaEditar->fk_id_dir) ? 'selected' : ''; ?>
                                            <option value="<?php echo $directores->fk_id_dir; ?>" <?php echo $selected; ?>><?php echo $directores->fk_id_dir; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                  <script type="text/javascript">
                                  $("#fk_id_dir").select2({
                                    maximumSelectionLength: 2
                                  });
                                  </script>
                                    <br>
                                    <div class="form-group">
                                      <label for="example-email" class="col-md-12">Nombre de revista </label>
                                      <div class="col-md-12">
                                          <input type="text" value="<?php echo $revistaEditar->nombre_rev; ?>" name="nombre_rev" id="nombre_rev" placeholder="Ingrese el nombre de la revista" class="form-control form-control-line">
                                      </div>
                                  </div>
                                  <script>
                                  document.getElementById('nombre_rev').onkeypress = function(e) {
                                    var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                    var charTyped = String.fromCharCode(charCode);
                                    var regex = /^[A-Za-záéíóúÁÉÍÓÚ\s]+$/;
                                    if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                      e.preventDefault();
                                    }
                                  };
                                  </script>
                                  <br>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <center>
                                              <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
                                              <a href="<?php echo site_url('revistas/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark"></i> &nbsp Cancelar</a>
                                          </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>

            <script type="text/javascript">
        $("#frm_nuevo_revista").validate({
          rules:{
            "fk_id_aut":{
              required:true
            },
            "fk_id_dir":{
              required:true
            },
            "nombre_rev":{
              required:true,
            }
          },
          messages:{
            "fk_id_aut":{
              required:"Debe seleccionar el id del autor"
            },
            "fk_id_dir":{
              required:"Debe seleccionar el id del director"
            },
            "nombre_rev":{
              required:"Ingrese el nombre d ela revista",
            }
          }
        });
      </script>
