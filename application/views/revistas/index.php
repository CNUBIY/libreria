<!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Revistas</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                            <li class="breadcrumb-item active">Revistas</li>
                        </ol>
                    </div>
                    <div class="col-md-6 col-4 align-self-center">
                        <a href="<?php echo site_url('revistas/nuevo') ?>" class="btn pull-right hidden-sm-down btn-success">Agregar Revista</a>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card">
                          <div class="card-block">
                              <h4 class="card-title">Revistas</h4>
                              <h6 class="card-subtitle">UTC -<code>Revistas</code></h6>
                              <div class="table-responsive">
                                  <?php if ($listadoRevistas): ?>
                                  <table class="table" id="tbl_2">
                                      <thead>
                                          <tr>
                                              <th>ID</th>
                                              <th>AUTOR</th>
                                              <th>DIRECTOR</th>
                                              <th>REVISTA</th>
                                              <th>Acciones</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <?php foreach ($listadoRevistas as $revista): ?>
                                          <tr>
                                              <td><?php echo $revista->id_rev; ?></td>
                                              <td><?php echo $revista->nombre_autor; ?></td>
                                              <td><?php echo $revista->nombre_dir; ?></td>
                                              <td><?php echo $revista->nombre_rev; ?></td>
                                              <td>
                                                  <a href="<?php echo site_url('revistas/editar/').$revista->id_rev; ?>" class="btn btn-warning" title="Editar"><i class="fa fa-pen"></i></a>
                                                  <a class="btn btn-danger delete-btn" href="<?php echo site_url('revistas/borrar/').$revista->id_rev; ?>" title="Eliminar" data-id_rev="<?php echo $revista->id_rev; ?>"><i class="fa fa-trash"></i></a>
                                              </td>
                                          </tr>
                                          <?php endforeach; ?>
                                      </tbody>
                                  </table>
                                  <?php else: ?>
                                  <div class="alert alert-danger">
                                      No se encontraron revistas registradas
                                  </div>
                                  <?php endif; ?>
                              </div><br>
                              <button id="toggleChartButton" onclick="toggleChart()" class="btn btn-outline-info">Mostrar/ocultar gráfico</button>
                              <div id="chartContainer" style="width: 50%; margin: auto; border: 1px solid black; padding: 10px; display: none;">
                                  <canvas id="myChart"></canvas>
                                  <canvas id="myPieChart"></canvas>
                              </div><br>
                          </div>

                          <script>
                              $(document).ready(function() {
                                  $('.delete-btn').click(function(event) {
                                      event.preventDefault();
                                      var id = $(this).data('id_rev');
                                      Swal.fire({
                                          title: '¿Estás seguro de que quieres eliminar este registro?',
                                          showDenyButton: true,
                                          showCancelButton: true,
                                          confirmButtonText: 'Sí',
                                          denyButtonText: 'No',
                                          customClass: {
                                              actions: 'my-actions',
                                              cancelButton: 'order-1 right-gap',
                                              confirmButton: 'order-2',
                                              denyButton: 'order-3',
                                          },
                                      }).then((result) => {
                                          if (result.isConfirmed) {
                                              window.location.href = "<?php echo site_url('revistas/borrar/'); ?>" + id;
                                          } else if (result.isDenied) {
                                              // No hacer nada si el usuario cancela la eliminación
                                          }
                                      });
                                  });
                              });
                          </script>
                          <script type="text/javascript">
                                  $(document).ready(function() {
                                      $('#tbl_2').DataTable( {
                                          dom: 'Bfrtip',
                                          buttons: [
                                              {
                                                  extend: 'pdfHtml5',
                                                  text: '<i class="fa-solid fa-file-pdf"></i> Exportar a PDF',
                                                  className: 'btn btn-outline-info',
                                                  messageTop: 'REPORTE DE MARCAS ',
                                                  title:'INFORMACIÓN Banco Solidario'
                                              },
                                              {
                                                  extend: 'print',
                                                  text: '<i class="fa-solid fa-print"></i> Imprimir',
                                                  className: 'btn btn-outline-info',
                                                  messageTop: 'REPORTE DE MARCAS ',
                                                  title:'INFORMACIÓN Banco Solidario'
                                              },
                                              {
                                                  extend: 'csv',
                                                  text: '<i class="fa-solid fa-file-csv"></i> Exportar a CSV',
                                                  className: 'btn btn-outline-info',
                                                  messageTop: 'REPORTE DE MARCAS ',
                                                  title:'INFORMACIÓN Banco Solidario'
                                              }
                                          ],
                                          language: {
                                              url: "https://cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json"
                                          }
                                      } );
                                  } );
                              </script>
