
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Agregar</h3>
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                          <li class="breadcrumb-item"><a href="<?php echo site_url('revistas/index'); ?>">Revistas</a></li>
                          <li class="breadcrumb-item active">Agregar Revistas</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <i class="fa-brands fa-stack-overflow m-r-10 fa-10x"></i>
                                    <h4 class="card-title m-t-10">Revistas</h4>
                                    <h6 class="card-subtitle">Registra la nueva revista</h6>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <form action="<?php echo site_url('revistas/guardarRevista'); ?>" id="frm_nuevo_revista"  enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                                  <div class="form-group">
                                      <label class="col-md-12">Autor</label>
                                      <select required name="fk_id_aut"  id="fk_id_aut" class="form-control form-control-line">
                                        <option value="">Escoge el id autor</option>
                                        <?php foreach ($listadoAutores as $autores): ?>
                                          <option value="<?php echo $autores->id_aut; ?>">
                                            <?php echo $autores->nombres_aut . ' ' .$autores->apellidos_aut; ?>
                                          </option>
                                        <?php endforeach; ?>
                                      </select>

                                  </div>
                                  <script type="text/javascript">
                                  $("#fk_id_aut").select2({
                                    maximumSelectionLength: 2
                                  });
                                  </script>
                                    <br>
                                    <div class="form-group">
                                        <label class="col-md-12">Directores</label>
                                        <select required name="fk_id_dir"  id="fk_id_dir" class="form-control form-control-line">
                                          <option value="">Escoge el id de director</option>
                                          <?php foreach ($listadoDirectores as $director): ?>
                                            <option value="<?php echo $director->id_dir; ?>"><?php echo $director->nombre_dir . ' ' .$director->apellido_dir; ?></option>
                                          <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <script type="text/javascript">
                                    $("#fk_id_dir").select2({
                                      maximumSelectionLength: 2
                                    });
                                    </script>
                                      <br>
                                      <div class="form-group">
                                          <label class="col-md-12">Nombre</label>
                                          <div class="col-md-12">
                                              <input required type="text" name="nombre_rev" id="nombre_rev" placeholder="Ingrese el nombre de la revista" class="form-control form-control-line">
                                          </div>
                                      </div>
                                      <script>
                                      document.getElementById('nombre_rev').onkeypress = function(e) {
                                        var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                        var charTyped = String.fromCharCode(charCode);
                                        var regex = /^[A-Za-záéíóúÁÉÍÓÚ\s]+$/;
                                        if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                          e.preventDefault();
                                        }
                                      };
                                      </script>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <center>
                                              <button type="submit" name="button" class="btn btn-outline-info">
                                                <i class="fa fa-save"></i>
                                                Guardar</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="<?php echo site_url('revistas/index'); ?>" class="btn btn-outline-danger">
                                                  <i class="fa fa-times"></i>
                                                  Cancelar</a>
                                          </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <script type="text/javascript">
        $("#frm_nuevo_revista").validate({
          rules:{
            "fk_id_aut":{
              required:true
            },
            "fk_id_dir":{
              required:true
            },
            "nombre_rev":{
            required:true,
          }
          },
          messages:{
            "fk_id_aut":{
              required:"Debe seleccionar el id del autor"
            },
            "fk_id_dir":{
              required:"Debe seleccionar el id del irector"
            },
            "nombre_rev":{
              required:"Ingrese el nombre",
            }
          }
        });
      </script>
