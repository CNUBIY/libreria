<!-- ============================================================== -->
			<!-- End Left Sidebar - style you can find in sidebar.scss  -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- Page wrapper  -->
			<!-- ============================================================== -->
			<div class="page-wrapper">
					<!-- ============================================================== -->
					<!-- Container fluid  -->
					<!-- ============================================================== -->
					<div class="container-fluid">
							<!-- ============================================================== -->
							<!-- Bread crumb and right sidebar toggle -->
							<!-- ============================================================== -->
							<div class="row page-titles">
									<div class="col-md-6 col-8 align-self-center">
											<h3 class="text-themecolor m-b-0 m-t-0">Cartas de Aceptación</h3>
											<ol class="breadcrumb">
													<li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
													<li class="breadcrumb-item active">Cartas de Aceptación</li>
											</ol>
									</div>
							</div>
							<!-- ============================================================== -->
							<!-- End Bread crumb and right sidebar toggle -->
							<!-- ============================================================== -->
							<!-- ============================================================== -->
							<!-- Start Page Content -->
							<!-- ============================================================== -->
							<!-- Row -->
							<div class="row">
									<div class="col-sm-12">
											<div class="card">
													<div class="card-block">
															<h4 class="card-title">Artículos</h4>
															<div class="table-responsive m-t-40">
																	<table class="table stylish-table" id="tbl_index">
																			<thead>
																					<tr>
																							<th>ID</th>
																							<th>Artículo</th>
																							<th>Fecha</th>
																							<th>Revista</th>
																							<th>Descargar</th>
																					</tr>
																			</thead>
																			<tbody>
																				<?php foreach ($listadoArticulos as $articulo): ?>
																				<tr>
																						<td><?php echo $articulo->id_art; ?></td>
																						<td><?php echo $articulo->titulo_art; ?></td>
																						<td><?php echo $articulo->fecha_art; ?></td>
																						<td><?php echo $articulo->nombre_rev; ?></td>
																						<td>
																								<a href="<?php echo site_url('welcome/reporte/'.$articulo->id_art); ?>">Vista Previa</a>
																						</td>
																				</tr>
																				<?php endforeach; ?>
																			</tbody>
																	</table>
															</div>
													</div>
											</div>
									</div>
							</div>
							<!-- Row -->
							<!-- Row -->

							<!-- Row -->
							<!-- ============================================================== -->
							<!-- End PAge Content -->
							<!-- ============================================================== -->
					</div>
					<!-- ============================================================== -->
					<!-- End Container fluid  -->
					<!-- ============================================================== -->
					<!-- ============================================================== -->

					<script type="text/javascript">
									$(document).ready(function() {
											$('#tbl_index').DataTable( {
													dom: 'Bfrtip',
													buttons: [
															{
																	extend: 'pdfHtml5',
																	text: '<i class="fa-solid fa-file-pdf"></i> Exportar a PDF',
																	className: 'btn btn-outline-info',
																	messageTop: 'REPORTE DE MARCAS ',
																	title:'INFORMACIÓN REPORTES'
															},
															{
																	extend: 'print',
																	text: '<i class="fa-solid fa-print"></i> Imprimir',
																	className: 'btn btn-outline-info',
																	messageTop: 'REPORTE DE MARCAS ',
																	title:'INFORMACIÓN REPORTES'
															},
															{
																	extend: 'csv',
																	text: '<i class="fa-solid fa-file-csv"></i> Exportar a CSV',
																	className: 'btn btn-outline-info',
																	messageTop: 'REPORTE DE MARCAS ',
																	title:'INFORMACIÓN REPORTES'
															}
													],
													language: {
															url: "https://cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json"
													}
											} );
									} );
							</script>
